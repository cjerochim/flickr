# React Development Test

Basic build making calls to the Flickr feed api. 

This application implements the "ducks-modular-redux" pattern
https://github.com/erikras/ducks-modular-redux

#Build dependencies
- parcel-bundler: simple tool for rapid development
- node-sass: handle scss

### Install dependencies
```
 npm install
```
### Development
```
 npm start
```
### Build Project
```
 npm run build
```