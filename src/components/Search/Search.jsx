import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tagValid } from '../../helpers';


import './Search.scss';


class Search extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSearchUpdate = this.onSearchUpdate.bind(this);
  }

  /**
   * Update state
   * @param {*} value 
   */
  onSearchUpdate({ target: { value } }) {
    const { updateTag } = this.props;
    updateTag(value);
  }

  /**
   * Update tag and clear field
   * @param {*} e 
   */

  onSubmit(e) {
    e.preventDefault();
    const { getFeedByTag, updateTag, tag } = this.props;
    getFeedByTag(tag);
  }

  render() {
    const { tag } = this.props;
    const isValid = tagValid(3);
    return (
      <div className="search">
        <form onSubmit={this.onSubmit}>
          <div className="search__group">
            <label className="search__label" htmlFor="search">Search for a tag</label>
            <input className="search__input" id="search" type="text" value={tag} onChange={this.onSearchUpdate} />
          </div>
          <div className="search__group">
            <button className="search__btn" disabled={isValid(tag)}>Search</button>
          </div>
        </form>
      </div>
    );
  }
}


Search.propTypes = {
  getFeedByTag: PropTypes.func.isRequired,
  updateTag: PropTypes.func.isRequired,
  tag: PropTypes.string.isRequired,
}

export default Search;