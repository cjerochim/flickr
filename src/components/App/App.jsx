import React, { Component } from 'react'; 
import { flickrGetFeed } from '../../helpers';

import Search from '../../containers/SearchContainer';
import Results from '../../containers/ResultsContainer';

import './App.scss';

export default () => {
  return (
    <div className="app">
        <header className="app__header">
          <Search />
        </header>
        <main className="app__main">
          <Results />
        </main>
      </div>
  );
}