import React from 'react';
import PropTypes from 'prop-types';

import './ResultsItem.scss';

const ResultsItem = ({ author, description, link, title, tags, media: { m:image } }) => {
  return (
    <aside className="results-item">
      <a href={link} className="results-item__image" target="_blank">
        <img src={image}  width="100%" />
      </a>
      <footer className="results-item__footer">
        <h2 className="results-item__title results-item__title--primary">{title}</h2>
        <h3 className="results-item__title results-item__title--secondary"><strong>Author</strong> - {author}</h3>
      </footer>
    </aside>
  );
};


ResultsItem.propTypes = {
  author: PropTypes.string.isRequired,
  description: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string.isRequired,
  tags: PropTypes.string,
  media: PropTypes.object.isRequired,
}

export default ResultsItem;