import React from 'react';
import PropTypes from 'prop-types';

import './Results.scss';

import ResultsItem from './ResultsItem';

const Results =  ({ items = [] }) => {
  if(items.length === 0) return null;
  return (
    <div className="results">
      <h2 className="results__title">Results</h2>
      <ul className="results__list">
        {items.map((item, index) => <ResultsItem key={index} {...item} />)}
      </ul>
    </div>
  );
};


Results.propTypes = {
  items: PropTypes.array.isRequired,
}

export default Results;