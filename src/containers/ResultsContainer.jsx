import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Search from '../components/Results/Results';

const mapStateToProps = ({ flickr }) => {
  return {
    items: flickr.items,
  }
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);