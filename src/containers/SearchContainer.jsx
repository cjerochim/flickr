import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Search from '../components/Search/Search';
import { getFeedByTag, updateTag  } from '../redux/modules/flickr';

const mapStateToProps = ({ flickr }) => {
  return {
    tag: flickr.tag
  }
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getFeedByTag,
    updateTag
  }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);