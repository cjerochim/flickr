import { combineReducers } from 'redux';

import flickr from './flickr';

export default combineReducers({
  flickr,
});