import { merge } from 'ramda';
import { flickrGetFeed, cleanTag } from '../../helpers';

export const LOAD = 'flickr/feed/LOAD';
export const SUCCESS = 'flickr/feed/SUCCESS';
export const TAG_UPDATE = 'flickr/feed/TAG_UPDATE';
export const ERROR = 'flickr/feed/ERROR';


const initialState = {
  isLoading: false,
  isError: false,
  tag: '',
  items: [],
}


export default (state = initialState, action = {}) => {
  const { type, payload } = action;
  switch(type) {
    case TAG_UPDATE:
      return merge(state, { tag: payload.tag });
    case LOAD:
      return merge(state, { isLoading: true, isError: false });
    case SUCCESS:
      return merge(state, { items: payload.items, isLoading: false });
    case ERROR:
      return merge(state, { isLoading: false, isError: true });
    default:
      return state;
  }
};


export const updateTag = (tag) => 
  ({ type: TAG_UPDATE, payload: { tag } });


export const getFeedByTag = (tag) => {
  return (dispatch) => {
    // Update state to reflect loading
    dispatch({ type: LOAD });
    // Format tag before passing it into the request
    const clean = cleanTag(tag);
    // Make request
    flickrGetFeed(clean)
      .then((res) => {
        const { items } = res;
        dispatch({ type: SUCCESS, payload: { items }});
      })
      .catch((err) => {
        dispatch({ type: ERROR, payload: {} })
        debugger;
      });
  }
};

