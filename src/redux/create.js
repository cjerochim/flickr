import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';


export default (initialData) => {
  const middleware = [thunk];
  const reducer = require('./modules/reducer').default;
  const store = createStore(reducer, initialData, composeWithDevTools(applyMiddleware(...middleware)));
  return store;
}