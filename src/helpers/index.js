import fetchJsonp from 'fetch-jsonp';
import { curry, trim, toLower, compose } from 'ramda';





export const flickrGetFeed = (tags) => {
  const url = `https://api.flickr.com/services/feeds/photos_public.gne?tags=${tags}&format=json`;
  return fetchJsonp(url, { jsonpCallbackFunction: 'jsonFlickrFeed', })
    .then(res => res.json());
};

export const cleanTag = compose(trim, toLower);

export const tagValid = curry((min, tag) => {
  if(min < tag.length) return false;
  return true;
});