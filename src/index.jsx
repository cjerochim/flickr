import React from 'react';
import { render } from 'react-dom';
import Root from './containers/RootContainer';

render(<Root />, document.getElementById('root'));